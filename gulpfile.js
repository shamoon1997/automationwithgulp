var gulp = require('gulp');
var minifycss = require('gulp-clean-css');
var minifyhtml = require('gulp-minify-html');
var imagemin = require('gulp-imagemin');


var uglify = require('gulp-uglify');


gulp.task('default', function() {
    console.log('Hello log')
});
gulp.task('imagemin', function() {
    return gulp.src('./src/*.jpg')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist'))

});

gulp.task('styles', function() {
    return gulp.src('./src/*.css')
        .pipe(minifycss())
        .pipe(gulp.dest('./dist'));


});

gulp.task('pages', function() {
    return gulp.src('./src/*.html')
        .pipe(minifyhtml())
        .pipe(gulp.dest('./dist'));

})

gulp.task('scripts', function() {
    return gulp.src('./src/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('./dist'));

});